/*
 * PHP verze 5.6, Mike Framework v.3
 *
 * @author Milan Kyncl <mmkyncl@gmail.com>, http://www.kyro.cz
 *
 * Copyright (c) 2016
 */

$(function(){
    $('[data-tab]').click(function(){
        $('[data-tab]').each(function(){
            $(this).removeClass('active');
        });
        $(this).addClass('active');
        $('.tab.active').removeClass('active');
        $('#' + $(this).data('tab')).addClass('active');

        return false;
    });
});
