$('[data-scroll-to]').click(function( event ) {
    var $anchor = $(this);

    $('html, body').stop().animate({
        scrollTop: ($($anchor.data('scroll-to')).offset().top)
    }, 850, 'easeInOutExpo', function(){
        window.setTimeout(function(){
            lockNavbar = false;
        }, 50);
    });
    event.preventDefault();
    return false;

});