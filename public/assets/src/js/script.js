var starsLinks;

(function ($) {

    starsLinks = $('.profile__info__rating__stars > a');

    $('.navbar__menu__toggle').click(function() {
        $('.user-menu').toggleClass('user-menu--open');
        $(this).toggleClass('navbar__menu__toggle--open');
    });

    starsLinks.hover(function() {
        var i = $(this).data('star');

        activateStars(i);
    });

    $('.profile__info__rating__stars').mouseleave(function() {
        deactivateStars();
    });

})(jQuery);

function activateStars( i ) {

    deactivateStars();

    starsLinks.each(function() {
        if(i > 0) {
            $(this).addClass('active');

            i--;
        }
    });
}

function deactivateStars() {
    $('.profile__info__rating__stars > a').removeClass('active');
}
