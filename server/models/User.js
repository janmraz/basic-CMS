var mongoose = require('mongoose');
const bcrypt = require( 'bcrypt-nodejs' );

module.exports.init = function () {
    var userSchema = new mongoose.Schema({
        email: String,
        created: { type: Date, default: Date.now },
        password: String,
        name: String,
        image: String,
        isAdmin: { type: Boolean, default: false }
    });
    // Make comparePassword helper method available
    userSchema.pre( 'save', function( next ) {
        // Create alias for current user model
        const user = this;

        // Generate salt
        bcrypt.genSalt( 10, function( err, salt ) {
            // Handle errors
            if ( err ) { return next( err ); }

            // Hash password + salt
            bcrypt.hash( user.password, salt, null, function( err, hash ) {
                // Handle errors
                if ( err ) { return next( err ); }

                user.password = hash;
                next();
            });
        });
    });

// Make comparePassword helper method available
    userSchema.methods.comparePassword = function( candidatePassword, callback ) {
        bcrypt.compare( candidatePassword, this.password, function( err, isMatch ) {
            // Handle errors
            if ( err ) { return callback( err ); }

            callback( null, isMatch );
        });
    };
    var User = mongoose.model('User', userSchema);


};
module.exports.dummy = function (cb) {
    const User = mongoose.model('User');
    User.findOne({},function (err,user) {
        if(err) throw err;
        console.log('Dummy data => email: ' + user === null ? user.email : ''  + ', password: '+ user === null ? user.password : '');
        if(user === null){
            let newUser = new User({
                email: 'tester@gmail.com',
                password: 'heslo123',
                name: "tester",
                isAdmin: true,
            });
            newUser.save(function (err) {
                if(err) throw err;
                console.log('added admin => ' + newUser.email + ' with password: ' + newUser.password);
            });
            let user = new User({
                email: 'user@gmail.com',
                name: "regular user",
                image: "/assets/build/img/foto.png",
                password: 'heslo123'
            });
            user.save(function (err,u) {
                if(err) throw err;
                console.log('added user => ' + u.email + ' with password: ' + u.password + ' with id ' + u._id);
                cb(u._id);
            });
        }
    });
};
