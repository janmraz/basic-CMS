var mongoose = require('mongoose');

module.exports.init = function () {
    var Schema = new mongoose.Schema({
        main_header: String,
        main_header_text: String,
        second_header: String,
        second_header_text: String,
        save_time: String,
        save_money: String,
        lectures_online: String,
        sliderInfo: [{name: String,years: String,title: String,text: String,image: String}],
        stories: [{name: String,text: String}],
        information_header: String,
        information_text: String,
        coaches: [{name: String,text: String, image: String}],
        coach_header: String,
        coach_subheader: String,
        footer_one: String,
        footer_two: String,

        titleOne: String,
        titleTwo: String,
        titleThree: String,
        titleFour: String,
        titleFive: String,
        encourage_header: String,
        encourage_text: String,

        team_header: String,
        team_text: String,
        info_header: String,
        info_text: String,
        uninfo_header: String,
        uninfo_text: String,
    });
    var Config = mongoose.model('Config', Schema);
    Config.findOne(function (err, config) {
        if(err) throw err;
        if(config === null){
            let config = new Config({
                main_header: "JEDINEČNÉ ON-LINE FITNESS!",
                main_header_text: "Nemáte čas chodit do posilovny? Tak cvičte z domova! V našem online fitku.",
                second_header: "UDĚLEJTE SI FITKO DOMA",
                second_header_text: "Nemáte žádnou posilovnu v okolí? Nestíháte nikam pravidelně docházet? Stydíte se cvičit před ostatními? Konec výmluv! Vezměte si na sebe ty nejpohodlnější tepláky a cvičte s námi online! Vysněnou postavu máte na dosah!",
                save_time: "Nemusíte nikam jezdit a cvičíte v pohodlí domova. Sledovat můžete pravidelné hodiny své oblíbené trenérky, nebo si vyberete z archivu.",
                save_money: "Platit můžete za každou lekci zvlášť nebo si pořídíte měsíční členství. Navíc si nemusíte kupovat nejmodernější sportovní oblečení. Cvičte v tom, v čem se cítíte nejlépe.",
                lectures_online: "Cvičit můžete odkudkoliv a kdykoliv chcete. Lekce jsou online a přehrajete je na mobilu, tabletu, počítači nebo televizi s online připojením.",
                sliderInfo: [
                    {name: "PŘÍBĚH HANKY Z BRNA",years: "19 let",title: "Hanka Novotná",text: "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Sed ut perspiciatis unde omnis iste natus error sit voluptatem.",image: "web/img/story-photo.jpg"},
                    {name: "PŘÍBĚH HANKY Z BRNA",years: "19 let",title: "Hanka Novotná",text: "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Sed ut perspiciatis unde omnis iste natus error sit voluptatem.",image: "web/img/story-photo.jpg"},
                    {name: "PŘÍBĚH HANKY Z BRNA",years: "19 let",title: "Hanka Novotná",text: "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Sed ut perspiciatis unde omnis iste natus error sit voluptatem.",image: "web/img/story-photo.jpg"},
                    {name: "PŘÍBĚH HANKY Z BRNA",years: "19 let",title: "Hanka Novotná",text: "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Sed ut perspiciatis unde omnis iste natus error sit voluptatem.",image: "web/img/story-photo.jpg"},
                ],
                stories: [
                    {name: "Postavíme Vám jídelníček na míru!",text: "Vyberte si a my Vám na základě volby vybereme vhodný cvíčící program, kter zlepší fyzickou kondici, postavu a celkově Vaše zdraví.Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Sed ut perspiciatis unde omnis iste natus error sit voluptatem."},
                    {name: "Postavíme Vám jídelníček na míru!",text: "Vyberte si a my Vám na základě volby vybereme vhodný cvíčící program, kter zlepší fyzickou kondici, postavu a celkově Vaše zdraví.Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Sed ut perspiciatis unde omnis iste natus error sit voluptatem."},
                    {name: "Postavíme Vám jídelníček na míru!",text: "Vyberte si a my Vám na základě volby vybereme vhodný cvíčící program, kter zlepší fyzickou kondici, postavu a celkově Vaše zdraví.Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Sed ut perspiciatis unde omnis iste natus error sit voluptatem."},
                    {name: "Postavíme Vám jídelníček na míru!",text: "Vyberte si a my Vám na základě volby vybereme vhodný cvíčící program, kter zlepší fyzickou kondici, postavu a celkově Vaše zdraví.Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Sed ut perspiciatis unde omnis iste natus error sit voluptatem."},
                ],
                information_header: "INFORMACE",
                information_text: "Fandíme vám a uděláme všechno proto, abyste dosáhli svých cílů a byli spokojení sami se sebou. Budeme vás motivovat, sledovat a radovat se z vašich úspěchů. Vyberte si typ cvičení a zvolte lektora. Typy cvičení můžete v průběhu členství libovolně kombinovat.",
                coaches: [
                    {name: "Jana Novotná",text: "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Sed ut perspiciatis unde omnis iste natus error sit voluptatem. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.", image: "web/img/story-photo.jpg"},
                    {name: "Jana Novotná",text: "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Sed ut perspiciatis unde omnis iste natus error sit voluptatem. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.", image: "web/img/story-photo.jpg"},
                    {name: "Jana Novotná",text: "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Sed ut perspiciatis unde omnis iste natus error sit voluptatem. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.", image: "web/img/story-photo.jpg"},
                    {name: "Jana Novotná",text: "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Sed ut perspiciatis unde omnis iste natus error sit voluptatem. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.", image: "web/img/story-photo.jpg"},
                ],
                coach_header: "NAŠI TRENÉŘI",
                coach_subheader: "Seznamte se s našimi trenéry, kteří s vámi budou cvičit.",
                footer_one: "Cras ligula felis, mattis a lobortis sed, consequat et erat. Nullam velit erat. hendrerit volutpat ex. Pellentesque bibendum et felis ac porttitor.",
                footer_two: "Cras ligula felis, mattis a lobortis sed, consequat et erat. Nullam velit erat. hendrerit volutpat ex. Pellentesque bibendum et felis ac porttitor.",
                titleOne: "Cum sociis natoque penatibus et magnis.",
                titleTwo: "Cum sociis natoque penatibus et magnis.",
                titleThree: "Cum sociis natoque penatibus et magnis.",
                titleFour: "Cum sociis natoque penatibus et magnis.",
                titleFive: "Cum sociis natoque penatibus et magnis.",
                encourage_header: "Stojí to za to?",
                encourage_text: "Děti teď prosedí 80% času z celého dne. Často mají nadváhu, se kterou pak bojují celý život.Máte doma dítě, které se nehne od tabletu nebo mobilu? A co kdyby se hýbalo s ním? Naše lekce jsou rozdělené do úrovní, takže si zacvičí i úplný začátečník.Trenéři jsou tentokrát děti. Náplň lekcí připravuje zkušená trenérka Petra Kadlecová Dočekalová, která vede velmi úspěšnou sportovní akademii.Udělejte pro budoucnost svých dětí to nejlepší a naučte je se o sebe starat. Díky tomu budou mít sebevědomí a dostatek energie na to, aby v životě uspěly.Prohlédněte si typy lekcí",
                team_header: "NÁŠ TÝM",
                team_text: "Představujeme tým našich lektorů, kteří vám pravidelně dávají do těla. :-)Můžete jim napsat email nebo se s nimi občas spojit na online chatu.",
                info_header: "INFORMACE",
                info_text: "Cras ligula felis, mattis a lobortis sed, consequat et erat. Nullam velit erat, sollicitudin ac orci a, henCras ligula felis, mattis a lobortis sed, consequat et erat. Cras ligula felis, mattis a lobortis sed, consequat et erat. Nullam velit erat, sollicitudin ac orci a, hendrerit volutpat ex. Pellentesque bibendum et felis ac porttitor. ue bibendum et felis ac porttitor.",
                uninfo_header: "VELIT ERAT, SOLLICITUDIN AC ORCIS",
                uninfo_text: "Cras ligula felis, mattis a lobortis sed, consequat et erat. Nullam velit erat, sollicitudin ac orci a, henCras ligula felis, mattis a lobortis sed, consequat et erat. Cras ligula felis, mattis a lobortis sed, consequat et erat. Nullam velit erat, sollicitudin ac orci a, hendrerit volutpat ex. Pellentesque bibendum et felis ac porttitor.",
            });
            config.save(function (err) {
                if(err) throw err;
                console.log('config saved');
            });
        }else{
            console.log('findOne');
        }
    })

};
