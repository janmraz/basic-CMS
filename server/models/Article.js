var mongoose = require('mongoose');

module.exports.init = function () {
    var ArticleSchema = new mongoose.Schema({
        author: String,
        header: String,
        photo: String,
        text: String,
        created: { type: Date, default: Date.now },
        updated: { type: Date, default: Date.now },
    });
    var Article = mongoose.model('Article', ArticleSchema);

};
module.exports.dummy = (id)=> {
    const Article = mongoose.model('Article');
    Article.findOne({},function (err,user) {
        if(err) throw err;
        if(user === null){
            let article = new Article({
                header: "ZA HRANICEMA MOŽNOSTÍ",
                text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
                photo: "/assets/build/img/article-photo.jpeg",
                author: id
            });
            let article2 = new Article({
                header: "ZA HRANICEMA MOŽNOSTÍ",
                text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
                photo: "/assets/build/img/article-photo.jpeg",
                author: id
            });
            article.save(function (err) {
                if(err) throw err;
                console.log('added article');
            });
            article2.save(function (err) {
                if(err) throw err;
                console.log('added article2');
            });
        }
    });
};