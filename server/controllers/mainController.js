'use strict';

const mongoose = require('mongoose');
const User = mongoose.model('User');
const Article = mongoose.model('Article');
const Config = mongoose.model('Config');

exports.getInfo = function (req, res) {
    let _id = req.params.id;
    User.findOne({ _id },function (err, user) {
        if(err) throw err;
        if(user === null) throw new Error("problem",user);
        res.json({data: user});
    });
};

exports.getConfig = function (req, res) {
    Config.findOne(function (err, config) {
        if(err) throw err;
        if(config === null) throw new Error("problem",config);
        res.json(config);
    });
};
exports.getArticles = function (req, res) {
    Article.find({}).lean().exec(function (err, articles) {
        if(err) throw err;
        if(articles === []) throw new Error("problem", articles);
        let u = [],result = [];
        articles.forEach((a)=>{
            u.push(a.author);
        });
        User.find({_id: u},function (err,users) {
            if(err) throw err;
            articles.forEach((a)=>{
                users.forEach((u)=>{
                    if(a.author == u._id){
                        a.user_name = u.name;
                        a.user_photo = u.image;
                    }
                });
            });
            res.json(articles);
        });


    });
};
exports.getArticle = function (req, res) {
    let _id = req.params.id;
    Article.findOne({ _id }).lean().exec(function (err, article) {
        if(err) throw err;
        if(article === null) throw new Error("problem",article);
        User.findOne({_id: article.author},function (err,user) {
            if(err) throw err;
            article.user_name = user.name;
            article.user_photo = user.image;
            res.json(article);
        });

    });
};
exports.editConfig = function (req, res) {
    let data = req.body.data;
    Config.findOne(function (err, config) {
        if (err) throw err;
        if (config === null) throw new Error("problem", config);
        console.log('coaches', data);
        config.main_header = data.main_header ? data.main_header : config.main_header;
        config.main_header_text = data.main_header_text ? data.main_header_text : config.main_header_text;
        config.second_header = data.second_header ? data.second_header : config.second_header;
        config.second_header_text = data.second_header_text ? data.second_header_text : config.second_header_text;
        config.save_time = data.save_time ? data.save_time : config.save_time;
        config.save_money = data.save_money ? data.save_money : config.save_money;
        config.lectures_online = data.lectures_online ? data.lectures_online : config.lectures_online;
        config.sliderInfo = data.sliderInfo ? data.sliderInfo : config.sliderInfo;
        config.stories = data.stories ? data.stories : config.stories;
        config.information_header = data.information_header ? data.information_header : config.information_header;
        config.information_text = data.information_text ? data.information_text : config.information_text;
        config.coaches = data.coaches ? data.coaches : config.coaches;
        config.coach_header = data.coach_header ? data.coach_header : config.coach_header;
        config.coach_subheader = data.coach_subheader ? data.coach_subheader : config.coach_subheader;
        config.configfooter_one = data.footer_one ? data.footer_one : config.footer_one;
        config.footer_two = data.footer_two ? data.footer_two : config.footer_two;
        config.titleOne = data.titleOne ? data.titleOne : '';
        config.titleTwo = data.titleTwo ? data.titleTwo : '';
        config.titleThree = data.titleThree ? data.titleThree : '';
        config.titleFour = data.titleFour ? data.titleFour : '';
        config.titleFive = data.titleFive ? data.titleFive : '';
        config.encourage_header = data.encourage_header ? data.encourage_header : '';
        config.encourage_text = data.encourage_text ? data.encourage_text : '';
        config.team_header = data.team_header ? data.team_header : '';
        config.team_text = data.team_text ? data.team_text : '';
        config.info_header = data.info_header ? data.info_header : '';
        config.info_text = data.info_text ? data.info_text : '';
        config.uninfo_header = data.uninfo_header ? data.uninfo_header : '';
        config.uninfo_text = data.uninfo_text ? data.uninfo_text : '';
        config.save(function (err, conf) {
            if (err) throw err;
            console.log('changed config');
            res.json(conf);
        });
    });
};
