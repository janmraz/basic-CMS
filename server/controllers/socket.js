const mongoose = require('mongoose');

module.exports = function(app) {
    let http = require('http').Server(app);
    let io = require('socket.io')(http);

    // start the server
    http.listen(app.get('port'), function() {
        console.log('Express server listening on port', app.get('port'));
    });

    module.exports.IO = io;
};
