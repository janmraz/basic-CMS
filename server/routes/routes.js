'use strict';

const cookieParser = require('cookie-parser');

// const cors = require('cors');

// cors setup
// var corsOptions = {
//   origin: 'www.anotherdomain.com'
// };

module.exports = function (app) {

    // use cookierParser
    app.use(cookieParser());

    // allow requests from cross origin
    // app.use(cors(corsOptions));
    // ---------- API -------------

    // USER ROUTES
    const api = require('./api');
    app.use('/api', api);

    app.get('/',function (req,res) {
        res.sendFile(process.cwd() + '/public/html/app.html');
    });
    app.get('/admin',function (req,res) {
        res.sendFile(process.cwd() + '/public/html/admin.html');
    });
    app.get('/admin/*',function (req,res) {
        res.sendFile(process.cwd() + '/public/html/admin.html');
    });
    app.get('/lobby',function (req,res) {
        res.sendFile(process.cwd() + '/public/html/lobby.html');
    });
    app.get('/lobby/*',function (req,res) {
        res.sendFile(process.cwd() + '/public/html/lobby.html');
    });
    app.get('/*',function (req,res) {
        res.sendFile(process.cwd() + '/public/html/app.html');
    });
};
