const express = require('express');
const router = express.Router();
const passport = require( 'passport' );

const Authentication = require( '../controllers/authentication' );
const passportService = require( '../services/passport' );


// controllers
const MainController = require('../controllers/mainController');
const requireAuth = passport.authenticate( 'jwt', { session: false });
const requireSignin = passport.authenticate( 'local', { session: false });




router.get('/user/config', MainController.getConfig);

router.get('/user/articles', MainController.getArticles);

router.get('/user/article/:id', MainController.getArticle);

router.post('/user/config',requireAuth, MainController.editConfig);

router.post('/admin/signin',requireSignin,Authentication.signinAdmin);
router.post('/user/signin',requireSignin,Authentication.signin);

router.get('/user/:id', MainController.getInfo);




module.exports = router;
