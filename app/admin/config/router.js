// Router set up
const React = require('react');
const {Route, Router, IndexRoute, browserHistory} = require('react-router');
let ls = require('local-storage');

// Components
import Auth from '../components/App/Auth';
import Home from '../components/Admin/Home';

import one from '../components/Admin/page/one';
import two from '../components/Admin/page/two';
import three from '../components/Admin/page/three';
import four from '../components/Admin/page/four';
import five from '../components/Admin/page/five';

// Routes
const routes = (
    <Router history={browserHistory}>
        <Route path='admin' component={Auth} />
    </Router>
);

const authorizedRoutes = (
    <Router history={browserHistory}>
        <Route path='admin' component={Home} >
            <IndexRoute  component={one}/>
            <Route path="typy-cviceni" component={two}/>
            <Route path="cviceni-s-detmi" component={three}/>
            <Route path="nas-tym" component={four}/>
            <Route path="kontakty" component={five}/>
        </Route>
    </Router>
);


export default ls('token') === null ? routes : ls('isAdmin') ? authorizedRoutes : routes;
