import {USER_LOGGED_OUT, USER_LOGIN, GET_USER, CHANGE_LOCATION, EDIT_USER,DELETE_USER} from '../actions/types.js';
let ls = require('local-storage');


export default function(state = {}, { type, payload }) {
    if (type === USER_LOGIN) {
        ls('token',payload.token);
        ls('isAdmin',true);
        ls('user.id',payload.data._id);
        location.reload();
        return payload.data
    }
    if (type === DELETE_USER) {
        ls('token',null);
        ls('user.email',null);
        ls('user.id',null);
        ls('isAdmin',null);
        return {}
    }
    if (type === USER_LOGGED_OUT) {
        ls('token',null);
        ls('user.email',null);
        ls('user.id',null);
        ls('isAdmin',null);
        return {}
    }
    if(type === GET_USER){
        return payload.data
    }
    if(type === EDIT_USER){
        return payload
    }
    return state;
}

