import { GET_CONFIG, EDIT_CONFIG } from '../actions/types.js';


export default function(state = {}, { type, payload }) {
    if (type === GET_CONFIG) {
        return payload
    }
    if (type === EDIT_CONFIG) {
        return payload
    }
    return state;
}

