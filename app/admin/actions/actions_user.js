import axios from 'axios';

// action types
import { USER_LOGIN } from './types';

// ----- THUNK ACTION CREATORS ------

export function logInUser(data) {
    return function(dispatch) {
        return axios.post('/api/admin/signin', data)
            .then((response) => {
                console.log('response from login',response);
                dispatch({
                    type: USER_LOGIN,
                    payload: response.data
                });
            })
            .catch((err) => {
                console.error("ERR",err);
            });
    };
}
