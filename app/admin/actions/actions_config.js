import axios from 'axios';
let ls = require('local-storage');

// action types
import { GET_CONFIG, EDIT_CONFIG } from './types';

export function getConfig() {
    return function(dispatch) {
        return axios.get('/api/user/config')
            .then((response) => {
                console.log(response.data);
                dispatch({
                    type: GET_CONFIG,
                    payload: response.data
                });
            })
            .catch((err) => {
                console.error("ERR",err);
            });
    };
}
export function editConfig(data) {
    return function(dispatch) {
        return axios.post('/api/user/config',{ data },{ headers: {'Authorization': ls('token')} })
            .then((response) => {
                console.log(response.data);
                dispatch({
                    type: EDIT_CONFIG,
                    payload: response.data
                });
            })
            .catch((err) => {
                console.error("ERR",err);
            });
    };
}

