// USER
export const USER_LOGIN = 'USER_LOGIN';

// ALERT
export const NEW_ALERT = 'NEW_ALERT';
export const REMOVE_ALERT = 'REMOVE_ALERT';

// CONFIG
export const GET_CONFIG = 'GET_CONFIG';
export const EDIT_CONFIG = 'EDIT_CONFIG';
