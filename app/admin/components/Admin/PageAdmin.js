const React = require('react');
import { connect } from 'react-redux';
import { getConfig,editConfig } from '../../actions/actions_config';


class PageOneAdmin extends React.Component {
    constructor(props){
        super(props);
    }
    componentDidMount(){
        this.props.dispatch(getConfig());
    }
    render() {
        let formOne = this.props.config.main_header != null ? this.props.children : <div>Loading ...</div>;
        return (
            <div className="content-wrapper admin-wrapper">
                <section className="content-header">
                    <h1>
                        Config
                    </h1>
                </section>
                <section className="content">
                    {formOne}
                </section>
            </div>
        );
    }
}

export default connect((state)=>{
    return {
        config: state.config,
    }
})(PageOneAdmin);
