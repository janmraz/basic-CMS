const React = require('react');
const {Link, IndexLink} = require('react-router');
import { connect } from 'react-redux';
import Header from './Header';
import SubMenu from './Menu';

class Home extends React.Component {
    render() {
        return (
            <div className="hold-transition skin-red sidebar-mini">
            <div className="admin-wrapper">
                <Header/>
                <SubMenu/>
                {this.props.children}
                <footer className="main-footer">
                    <div className="pull-right hidden-xs">
                        Anything you want
                    </div>
                    <strong>Copyright &copy; 2015 <a href="#">Company</a>.</strong> All rights reserved.
                </footer>

                <div className="control-sidebar-bg"></div>
            </div>
            </div>
        );
    }
}
export default Home;
