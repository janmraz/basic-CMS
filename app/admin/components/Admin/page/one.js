import React from 'react';
import { connect } from 'react-redux';
import Table from '../Table';

import { getConfig,editConfig } from '../../../actions/actions_config';


class One extends React.Component {

    constructor(){
        super();
        this.handleChange = this.handleChange.bind(this);
        this.deleteItem = this.deleteItem.bind(this);
        this.addItem = this.addItem.bind(this);
        this.editItem = this.editItem.bind(this);
        this.sendData = this.sendData.bind(this);
        this.state = {
            main_header: '',
            main_header_text: '',
            second_header: '',
            second_header_text: '',
            save_time: '',
            save_money: '',
            lectures_online: '',
            sliderInfo: [],
            stories: [],
            information_header: '',
            information_text: '',
            coaches: [],
            coach_header: '',
            coach_subheader: '',
            footer_one: '',
            footer_two: '',
            titleOne:'',
            titleTwo:'',
            titleThree:'',
            titleFour:'',
            titleFive:'',
            encourage_header: '',
            encourage_text: '',
            team_header: '',
            team_text: '',
            info_header: '',
            info_text: '',
            uninfo_header: '',
            uninfo_text: '',
            loaded: false
        }
    }
    handleChange(event,type) {
        switch (type){
            case 'main_header':
                this.setState({main_header: event.target.value});
                break;
            case 'main_header_text':
                this.setState({main_header_text: event.target.value});
                break;
            case 'second_header':
                this.setState({second_header: event.target.value});
                break;
            case 'second_header_text':
                this.setState({second_header_text: event.target.value});
                break;
            case 'save_time':
                this.setState({save_time: event.target.value});
                break;
            case 'save_money':
                this.setState({save_money: event.target.value});
                break;
            case 'lectures_online':
                this.setState({lectures_online: event.target.value});
                break;
            case 'information_header':
                this.setState({information_header: event.target.value});
                break;
            case 'information_text':
                this.setState({information_text: event.target.value});
                break;
            case 'coach_header':
                this.setState({coach_header: event.target.value});
                break;
            case 'coach_subheader':
                this.setState({coach_subheader: event.target.value});
                break;
            case 'footer_one':
                this.setState({footer_one: event.target.value});
                break;
            case 'footer_two':
                this.setState({footer_two: event.target.value});
                break;
            case 'titleOne':
                this.setState({titleOne: event.target.value});
                break;
            case 'titleTwo':
                this.setState({titleTwo: event.target.value});
                break;
            case 'titleThree':
                this.setState({titleThree: event.target.value});
                break;
            case 'titleFour':
                this.setState({titleFour: event.target.value});
                break;
            case 'titleFive':
                this.setState({titleFive: event.target.value});
                break;
            case 'encourage_header':
                this.setState({encourage_header: event.target.value});
                break;
            case 'encourage_text':
                this.setState({encourage_text: event.target.value});
                break;
            case 'team_header':
                this.setState({team_header: event.target.value});
                break;
            case 'team_text':
                this.setState({team_text: event.target.value});
                break;
            case 'info_header':
                this.setState({info_header: event.target.value});
                break;
            case 'info_text':
                this.setState({info_text: event.target.value});
                break;
            case 'uninfo_header':
                this.setState({uninfo_header: event.target.value});
                break;
            case 'uninfo_text':
                this.setState({uninfo_text: event.target.value});
                break;
            default:
                console.log('err');
        }

    }
    componentDidMount(){
        console.log('props-form',this.props);
        let _this = this;
        this.props.dispatch(getConfig()).then(()=>{
            _this.setState({
                main_header: this.props.data.main_header ? this.props.data.main_header : '',
                main_header_text: this.props.data.main_header_text ? this.props.data.main_header_text : '',
                second_header: this.props.data.second_header ? this.props.data.second_header : '',
                second_header_text: this.props.data.second_header_text ? this.props.data.second_header_text : '',
                save_time: this.props.data.save_time ? this.props.data.save_time : '',
                save_money: this.props.data.save_money ? this.props.data.save_money : '',
                lectures_online: this.props.data.lectures_online ? this.props.data.lectures_online : '',
                sliderInfo: this.props.data.sliderInfo ? this.props.data.sliderInfo : [],
                stories: this.props.data.stories? this.props.data.stories : [],
                information_header: this.props.data.information_header ? this.props.data.information_header : '',
                information_text: this.props.data.information_text ? this.props.data.information_text : '',
                coaches: this.props.data.coaches ? this.props.data.coaches : [],
                coach_header: this.props.data.coach_header ? this.props.data.coach_header : '',
                coach_subheader: this.props.data.coach_subheader ? this.props.data.coach_subheader : '',
                footer_one: this.props.data.footer_one ? this.props.data.footer_one : '',
                titleOne: this.props.data.titleOne ? this.props.data.titleOne : '',
                titleTwo: this.props.data.titleTwo ? this.props.data.titleTwo : '',
                titleThree: this.props.data.titleThree ? this.props.data.titleThree : '',
                titleFour: this.props.data.titleFour ? this.props.data.titleFour : '',
                titleFive: this.props.data.titleFive ? this.props.data.titleFive : '',
                encourage_header: this.props.data.encourage_header ? this.props.data.encourage_header : '',
                encourage_text: this.props.data.encourage_text ? this.props.data.encourage_text : '',
                team_header: this.props.data.team_header ? this.props.data.team_header : '',
                team_text: this.props.data.team_text ? this.props.data.team_text : '',
                info_header: this.props.data.info_header ? this.props.data.info_header :'',
                info_text: this.props.data.info_text ? this.props.data.info_text : '',
                uninfo_header: this.props.data.uninfo_header ? this.props.data.uninfo_header : '',
                uninfo_text: this.props.data.uninfo_text ? this.props.data.uninfo_text : '',
                loaded: true
            });
        });
    }
    deleteItem(item,type){
        let _this = this;
        switch (type){
            case 'sliderInfo':
                let obj = _this.state.sliderInfo;
                delete obj[obj.indexOf(item)];
                this.setState({sliderInfo: obj.filter((item)=>{ if(item) return item })});
                break;
            case 'stories':
                let obj2 = _this.state.stories;
                delete obj2[obj2.indexOf(item)];
                this.setState({stories: obj2.filter((item)=>{ if(item) return item })});
                break;
            case 'coaches':
                let obj3 = _this.state.coaches;
                delete obj3[obj3.indexOf(item)];
                this.setState({coaches: obj3.filter((item)=>{ if(item) return item })});
                break;
            default:
                console.log('err');
        }
    }
    addItem(state){
        let _this = this;
        switch (state.collection){
            case 'sliderInfo':
                let obj4 = _this.state.sliderInfo;
                obj4.push({name: state.name,text: state.text});
                this.setState({sliderInfo: obj4});
                break;
            case 'stories':
                let obj5 = _this.state.stories;
                obj5.push({name: state.name,text: state.text});
                this.setState({stories: obj5});
                break;
            case 'coaches':
                let obj6 = _this.state.coaches;
                obj6.push({name: state.name,text: state.text});
                this.setState({coaches: obj6});
                break;
            default:
                console.log('err');
        }
    }
    editItem(state){
        let realstate = Object.assign({}, this.state);
        switch (state.collection){
            case 'sliderInfo':
                realstate.sliderInfo[state.key].text = state.text;
                realstate.sliderInfo[state.key].name = state.name;
                this.setState(realstate);
                break;
            case 'stories':
                realstate.stories[state.key].text = state.text;
                realstate.stories[state.key].name = state.name;
                this.setState(realstate);
                break;
            case 'coaches':
                realstate.coaches[state.key].text = state.text;
                realstate.coaches[state.key].name = state.name;
                this.setState(realstate);
                break;
            default:
                console.log('err');
        }
    }
    sendData(){
        console.log('state send',this.state);
        this.props.dispatch(editConfig(this.state));
    }
    render(){
        let slider = this.state.loaded ? <div><Table data={this.state.sliderInfo} collection="sliderInfo" name="Slider Info" addItem={this.addItem} editItem={this.editItem} deleteItem={this.deleteItem}/></div> : <div>Loading...</div>;
        let stories = this.state.loaded ? <div><Table data={this.state.stories} collection="stories" name=" Stories"addItem={this.addItem} editItem={this.editItem} deleteItem={this.deleteItem}/></div> : <div>Loading...</div>;
        let coaches = this.state.loaded ? <div><Table data={this.state.coaches} collection="coaches" name="Coaches" addItem={this.addItem} editItem={this.editItem} deleteItem={this.deleteItem}/></div> : <div>Loading...</div>;
        return(
            <div className="content-wrapper admin-wrapper">
                <section className="content-header">
                    <h1>
                        Cvič doma
                    </h1>
                </section>
                <section className="content">
                <div className="form-group"><label>Main Header</label><input className="form-control" value={this.state.main_header} onChange={(event) => this.handleChange(event,'main_header')}/></div>
                <div className="form-group"><label>Main Header text</label><input className="form-control" value={this.state.main_header_text} onChange={(event) => this.handleChange(event,'main_header_text')}/></div>
                <div className="form-group"><label>Second Header</label><input className="form-control"  value={this.state.second_header} onChange={(event) => this.handleChange(event,'second_header')}/></div>
                <div className="form-group"><label>Second Header text</label><input className="form-control" value={this.state.second_header_text} onChange={(event) => this.handleChange(event,'second_header_text')}/></div>
                <div className="form-group"><label>Save money text</label><textarea rows="3" className="form-control" value={this.state.save_money} onChange={(event) => this.handleChange(event,'save_money')}/></div>
                <div className="form-group"><label>Save time text</label><textarea rows="3" className="form-control" value={this.state.save_time} onChange={(event) => this.handleChange(event,'save_time')}/></div>
                <div className="form-group"><label>Lectures online text</label><textarea rows="3" className="form-control" value={this.state.lectures_online} onChange={(event) => this.handleChange(event,'lectures_online')}/></div>
                <br />
                <br />
                {slider}
                <br />
                <br />
                <div className="form-group"><label>Information Header</label><input className="form-control" value={this.state.information_header} onChange={(event) => this.handleChange(event,'information_header')}/></div>
                <div className="form-group"><label>Information Text</label><textarea className="form-control" rows="3" value={this.state.information_text} onChange={(event) => this.handleChange(event,'information_text')}/></div>
                <br />
                <br />
                {stories}
                <br />
                <br />
                {coaches}
                <br />
                <br />
                <div className="form-group"><label>Coach Header</label><input className="form-control" value={this.state.coach_header} onChange={(event) => this.handleChange(event,'coach_header')}/></div>
                <div className="form-group"><label>Coach Sub-Header</label><input className="form-control" value={this.state.coach_subheader} onChange={(event) => this.handleChange(event,'coach_subheader')}/></div>
                <button type="button" className="btn btn-block btn-success btn-lg" onClick={this.sendData}>Save</button>
    </section>
    </div>)
    }
}

export default connect((state)=>{
    return {
        data: state.config,
    }
})(One);