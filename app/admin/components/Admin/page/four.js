import React from 'react';
import { connect } from 'react-redux';

import { getConfig,editConfig } from '../../../actions/actions_config';


class Four extends React.Component {

    constructor(){
        super();
        this.handleChange = this.handleChange.bind(this);
        this.sendData = this.sendData.bind(this);
        this.state = {
            main_header: '',
            main_header_text: '',
            second_header: '',
            second_header_text: '',
            save_time: '',
            save_money: '',
            lectures_online: '',
            sliderInfo: [],
            stories: [],
            information_header: '',
            information_text: '',
            coaches: [],
            coach_header: '',
            coach_subheader: '',
            footer_one: '',
            footer_two: '',
            titleOne:'',
            titleTwo:'',
            titleThree:'',
            titleFour:'',
            titleFive:'',
            encourage_header: '',
            encourage_text: '',
            team_header: '',
            team_text: '',
            info_header: '',
            info_text: '',
            uninfo_header: '',
            uninfo_text: '',
            loaded: false
        }
    }
    handleChange(event,type) {
        switch (type){
            case 'main_header':
                this.setState({main_header: event.target.value});
                break;
            case 'main_header_text':
                this.setState({main_header_text: event.target.value});
                break;
            case 'second_header':
                this.setState({second_header: event.target.value});
                break;
            case 'second_header_text':
                this.setState({second_header_text: event.target.value});
                break;
            case 'save_time':
                this.setState({save_time: event.target.value});
                break;
            case 'save_money':
                this.setState({save_money: event.target.value});
                break;
            case 'lectures_online':
                this.setState({lectures_online: event.target.value});
                break;
            case 'information_header':
                this.setState({information_header: event.target.value});
                break;
            case 'information_text':
                this.setState({information_text: event.target.value});
                break;
            case 'coach_header':
                this.setState({coach_header: event.target.value});
                break;
            case 'coach_subheader':
                this.setState({coach_subheader: event.target.value});
                break;
            case 'footer_one':
                this.setState({footer_one: event.target.value});
                break;
            case 'footer_two':
                this.setState({footer_two: event.target.value});
                break;
            case 'titleOne':
                this.setState({titleOne: event.target.value});
                break;
            case 'titleTwo':
                this.setState({titleTwo: event.target.value});
                break;
            case 'titleThree':
                this.setState({titleThree: event.target.value});
                break;
            case 'titleFour':
                this.setState({titleFour: event.target.value});
                break;
            case 'titleFive':
                this.setState({titleFive: event.target.value});
                break;
            case 'encourage_header':
                this.setState({encourage_header: event.target.value});
                break;
            case 'encourage_text':
                this.setState({encourage_text: event.target.value});
                break;
            case 'team_header':
                this.setState({team_header: event.target.value});
                break;
            case 'team_text':
                this.setState({team_text: event.target.value});
                break;
            case 'info_header':
                this.setState({info_header: event.target.value});
                break;
            case 'info_text':
                this.setState({info_text: event.target.value});
                break;
            case 'uninfo_header':
                this.setState({uninfo_header: event.target.value});
                break;
            case 'uninfo_text':
                this.setState({uninfo_text: event.target.value});
                break;
            default:
                console.log('err');
        }

    }
    componentDidMount(){
        console.log('props-form',this.props);
        let _this = this;
        this.props.dispatch(getConfig()).then(()=>{
            _this.setState({
                main_header: this.props.data.main_header ? this.props.data.main_header : '',
                main_header_text: this.props.data.main_header_text ? this.props.data.main_header_text : '',
                second_header: this.props.data.second_header ? this.props.data.second_header : '',
                second_header_text: this.props.data.second_header_text ? this.props.data.second_header_text : '',
                save_time: this.props.data.save_time ? this.props.data.save_time : '',
                save_money: this.props.data.save_money ? this.props.data.save_money : '',
                lectures_online: this.props.data.lectures_online ? this.props.data.lectures_online : '',
                sliderInfo: this.props.data.sliderInfo ? this.props.data.sliderInfo : [],
                stories: this.props.data.stories? this.props.data.stories : [],
                information_header: this.props.data.information_header ? this.props.data.information_header : '',
                information_text: this.props.data.information_text ? this.props.data.information_text : '',
                coaches: this.props.data.coaches ? this.props.data.coaches : [],
                coach_header: this.props.data.coach_header ? this.props.data.coach_header : '',
                coach_subheader: this.props.data.coach_subheader ? this.props.data.coach_subheader : '',
                footer_one: this.props.data.footer_one ? this.props.data.footer_one : '',
                titleOne: this.props.data.titleOne ? this.props.data.titleOne : '',
                titleTwo: this.props.data.titleTwo ? this.props.data.titleTwo : '',
                titleThree: this.props.data.titleThree ? this.props.data.titleThree : '',
                titleFour: this.props.data.titleFour ? this.props.data.titleFour : '',
                titleFive: this.props.data.titleFive ? this.props.data.titleFive : '',
                encourage_header: this.props.data.encourage_header ? this.props.data.encourage_header : '',
                encourage_text: this.props.data.encourage_text ? this.props.data.encourage_text : '',
                team_header: this.props.data.team_header ? this.props.data.team_header : '',
                team_text: this.props.data.team_text ? this.props.data.team_text : '',
                info_header: this.props.data.info_header ? this.props.data.info_header :'',
                info_text: this.props.data.info_text ? this.props.data.info_text : '',
                uninfo_header: this.props.data.uninfo_header ? this.props.data.uninfo_header : '',
                uninfo_text: this.props.data.uninfo_text ? this.props.data.uninfo_text : '',
                loaded: true
            });
        });
    }
    sendData(){
        console.log('state send',this.state);
        this.props.dispatch(editConfig(this.state));
    }
    render(){
        return(
            <div className="content-wrapper admin-wrapper">
                <section className="content-header">
                    <h1>
                        Náš tým
                    </h1>
                </section>
                <section className="content">
                    <div className="form-group"><label>Team Header</label><input className="form-control" value={this.state.team_header} onChange={(event) => this.handleChange(event,'team_header')}/></div>
                    <div className="form-group"><label>Team Text</label><input className="form-control" value={this.state.team_text} onChange={(event) => this.handleChange(event,'team_text')}/></div>
                    <div className="form-group"><label>Info Header</label><input className="form-control" value={this.state.info_header} onChange={(event) => this.handleChange(event,'info_header')}/></div>
                    <div className="form-group"><label>Info Text</label><input className="form-control" value={this.state.info_text} onChange={(event) => this.handleChange(event,'info_text')}/></div>
                    <div className="form-group"><label>Uninfo Header</label><input className="form-control" value={this.state.uninfo_header} onChange={(event) => this.handleChange(event,'uninfo_header')}/></div>
                    <div className="form-group"><label>Uninfo Text</label><input className="form-control" value={this.state.uninfo_text} onChange={(event) => this.handleChange(event,'uninfo_text')}/></div>
                    <button type="button" className="btn btn-block btn-success btn-lg" onClick={this.sendData}>Save</button>
                </section>
            </div>)
    }
}

export default connect((state)=>{
    return {
        data: state.config,
    }
})(Four);