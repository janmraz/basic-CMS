const React = require('react');
const ls = require('local-storage');
const {Link, IndexLink} = require('react-router');

class Header extends React.Component {

    render() {
        return (
            <header className="main-header">

                <a href="index2.html" className="logo">
                    <span className="logo-mini"><b>A</b>LT</span>
                    <span className="logo-lg"><b>Admin</b>LTE</span>
                </a>

                <nav className="navbar navbar-static-top" role="navigation">
                    <a href="#" className="sidebar-toggle" data-toggle="offcanvas" role="button">
                        <span className="sr-only">Toggle navigation</span>
                    </a>
                    <div className="navbar-custom-menu">
                        <ul className="nav navbar-nav">
                            <li className="dropdown user user-menu">
                                <a onClick={() => {ls('token',null);ls('isAdmin',null);window.location = '/';}} className="dropdown-toggle" data-toggle="dropdown">
                                    <span className="hidden-xs">Logout</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </header>
        );
    }
}

export default Header;
