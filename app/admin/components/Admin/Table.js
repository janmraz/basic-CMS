const React = require('react');
const {Link, IndexLink} = require('react-router');
import Header from './Header';
import SubMenu from './Menu';

class Form extends React.Component {
    constructor(){
        super();
        this.handleChange = this.handleChange.bind(this);
        this.change = this.change.bind(this);
        this.revert = this.revert.bind(this);
        this.state = {
            key: '',
            name: '',
            text: '',
            collection: ''
        }
    }
    handleChange(key) {
        let _this = this;
        this.props.data.forEach(function (item,i) {
            if(i === key){
                _this.setState({name: item.name,text: item.text,key: key})
            }
        })
    }
    change(event,type){
        switch (type) {
            case 'name':
                this.setState({name: event.target.value});
                break;
            case 'text':
                this.setState({text: event.target.value});
                break;
            default:
                console.log('err');
        }
    }

    componentDidMount(){
        console.log('props',this.props);
        this.setState({
            collection: this.props.collection
        });
    }
    revert(){
        let revertobj =  {
            key: '',
            name: '',
            text: '',
        };
        this.setState(revertobj);
    }
    render() {
        let classNamesADD = this.state.name === '' || this.state.text === '' ? "btn btn-success disabled" : "btn btn-success";
        let classNamesEDIT = this.state.name === '' || this.state.text === '' ? "btn btn-primary disabled" : "btn btn-primary";
        let action = this.state.key === '' ? <button className={classNamesADD} onClick={()=>{this.props.addItem(this.state);this.revert()}}>add</button> : <button className={classNamesEDIT} onClick={()=>{this.props.editItem(this.state);this.revert()}}>edit</button>
        return (
            <div>
                <h3>{this.props.name}</h3>
                {this.props.data.map((item,i)=>{
                    return <div key={i}>
                        <h4>{item.name}</h4>
                        <h5>{item.text}</h5>
                        <button className="btn btn-info btn-xs" onClick={() => this.handleChange(i)}>Edit</button>
                        <button className="btn btn-danger btn-xs" onClick={() => {this.props.deleteItem(item,this.props.collection);this.revert();}}>Delete</button>
                        </div>
                })}
                <br />
                <br />
                <div>
                    <div className="form-group"><label>Name</label><input className="form-control" value={this.state.name} onChange={(event) => this.change(event,'name')}/></div>
                    <div className="form-group"><label>Text</label><textarea className="form-control" value={this.state.text} onChange={(event) => this.change(event,'text')}/></div>
                    <div>{action}</div>
                </div>
            </div>
        );
    }
}

export default Form;
