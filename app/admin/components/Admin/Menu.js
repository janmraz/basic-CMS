const React = require('react');
const {Link, IndexLink} = require('react-router');

class Menu extends React.Component {

    render() {
        return (
            <aside className="main-sidebar">
                <section className="sidebar">
                    <ul className="sidebar-menu">
                        <li className="header">MENU</li>
                        <li className={window.location.pathname === "/admin/" ?"active": ""}><Link to="/admin/"><i className="fa fa-link"></i> <span>Cvič doma</span></Link></li>
                        <li className={window.location.pathname === "/admin/typy-cviceni" ?"active": ""}><Link to="/admin/typy-cviceni"><i className="fa fa-link"></i> <span>Typy cvičení</span></Link></li>
                        <li className={window.location.pathname === "/admin/cviceni-s-detmi" ?"active": ""}><Link to="/admin/cviceni-s-detmi"><i className="fa fa-link"></i> <span>Cvičení s dětmi</span></Link></li>
                        <li className={window.location.pathname === "/admin/nas-tym" ?"active": ""}><Link to="/admin/nas-tym"><i className="fa fa-link"></i> <span>Náš tým</span></Link></li>
                        <li className={window.location.pathname === "/admin/kontakty" ?"active": ""}><Link to="/admin/kontakty"><i className="fa fa-link"></i> <span>Kontakty</span></Link></li>
                    </ul>
                </section>
            </aside>
        );
    }
}

export default Menu;
