const React = require('react');
const {Link, IndexLink} = require('react-router');

class Nav extends React.Component {

    render() {
        return (
            <nav className="navbar navbar-default navbar-fixed-top">
                <div className="container">
                    <div className="navbar-header">
                        <button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                            <span className="sr-only">Navigace</span>
                            <span className="icon-bar"></span>
                            <span className="icon-bar"></span>
                            <span className="icon-bar"></span>
                        </button>
                        <a className="navbar-brand" href="/"><img src="web/img/logo/logo-text.png" /></a>
                    </div>
                    <div id="navbar" className="navbar-collapse collapse">
                        <ul className="nav navbar-nav navbar-right">

                            <li><Link to="/" className={window.location.pathname === '/' ? "active" : ""}>Cvič doma</Link></li>
                            <li><Link to="/typy-cviceni" className={window.location.pathname === '/typy-cviceni' ? "active" : ""}>Typy cvičení</Link></li>
                            <li><Link to="/cviceni-s-detmi" className={window.location.pathname === '/cviceni-s-detmi' ? "active" : ""}>Cvičení s dětmi</Link></li>
                            <li><Link to="/nas-team" className={window.location.pathname === '/nas-team' ? "active" : ""}>Náš tým</Link></li>
                            <li><Link to="/kontakty" className={window.location.pathname === '/kontakty' ? "active" : ""}>Kontakty</Link></li>

                            <li className="dropdown">
                                <Link to="/admin" className="user-link dropdown-toggle" type="button" id="user-menu-dropdown"><img src="web/img/user-icon.png" /></Link>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
        );
    }
}

export default Nav;
