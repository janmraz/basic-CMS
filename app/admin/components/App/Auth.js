const React = require('react');
import { logInUser } from '../../actions/actions_user'
import { connect } from 'react-redux';
let ls = require('local-storage');

class Auth extends React.Component {
    constructor(){
        super();
        this.changeValue = this.changeValue.bind(this);
        this.send = this.send.bind(this);
        this.state = {
            email: '',
            password: ''
        }
    }
    changeValue(event,type){
        if(type === 'email'){
            this.setState({email: event.target.value});
        }else if(type === 'password'){
            this.setState({password: event.target.value});
        }
    }

    send(){
        let obj = {email: this.state.email, password: this.state.password};
        // if(this.state.email === 'tester@gmail.com' && this.state.password === 'heslo123'){
        //     ls('token','dummyToken123456789');
        // }
        this.props.dispatch(logInUser(obj));
    }

    render(){
        return (
            <div className="hold-transition login-page">
            <div className="login-box">
                <div className="login-logo">
                    <a href="../../index2.html"><b>Admin</b>LTE</a>
                </div>
                <div className="login-box-body">
                    <p className="login-box-msg">Sign in to start your session</p>

                    <form onSubmit={(e)=>{e.preventDefault()}}>
                        <div className="form-group has-feedback">
                            <input type="email" className="form-control" placeholder="Email" value={this.state.email} onChange={(event)=> this.changeValue(event,'email')} />
                                <span className="glyphicon glyphicon-envelope form-control-feedback"></span>
                        </div>
                        <div className="form-group has-feedback">
                            <input type="password" className="form-control" placeholder="Password" value={this.state.password} onChange={(event)=> this.changeValue(event,'password')} />
                                <span className="glyphicon glyphicon-lock form-control-feedback"></span>
                        </div>
                        <div className="row">
                            <div className="col-xs-4">
                                <button type="submit" className="btn btn-primary btn-block btn-flat" onClick={this.send}>Sign In</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            </div>
        );
    }
}


export default connect()(Auth);
