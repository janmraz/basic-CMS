// USER
export const USER_LOGGED_IN = 'USER_LOGGED_IN';
export const USER_LOGIN = 'USER_LOGIN';
export const USER_LOGGED_OUT = 'USER_LOGGED_OUT';
export const GET_USER = 'GET_USER';
export const FETCH_USER = 'FETCH_USER';
export const CHANGE_LOCATION = 'CHANGE_LOCATION';
export const CHANGE_SEARCH = 'CHANGE_SEARCH';
export const EDIT_USER = 'EDIT_USER';
export const DELETE_USER = 'DELETE_USER';

// ALERT
export const NEW_ALERT = 'NEW_ALERT';
export const REMOVE_ALERT = 'REMOVE_ALERT';

// CONFIG
export const GET_CONFIG = 'GET_CONFIG';
export const EDIT_CONFIG = 'EDIT_CONFIG';
