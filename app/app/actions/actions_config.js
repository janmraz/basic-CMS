import axios from 'axios';

// action types
import { GET_CONFIG } from './types';

export function getConfig() {
    return function(dispatch) {
        return axios.get('/api/user/config')
            .then((response) => {
                console.log('actions_config',response.data);
                dispatch({
                    type: GET_CONFIG,
                    payload: response.data
                });
            })
            .catch((err) => {
                console.error("ERR",err);
            });
    };
}

