const React = require('react');

class Footer extends React.Component {
    render(){
        return (
            <footer>
                <div className="top">
                    <div className="container">
                        <div className="col-md-5">
                            <div className="product">
                                <img src="/web/img/logo/logo-text-white.png" width="160" />
                                    <p>
                                        Cras ligula felis, mattis a lobortis sed, consequat et erat. Nullam velit erat. hendrerit volutpat ex. Pellentesque bibendum et felis ac porttitor.
                                    </p>
                            </div>

                            <div className="product">
                                <img src="/web/img/logo/20-min-white.png" width="250" />
                                    <p>
                                        Cras ligula felis, mattis a lobortis sed, consequat et erat. Nullam velit erat. hendrerit volutpat ex. Pellentesque bibendum et felis ac porttitor.
                                    </p>
                            </div>
                        </div>
                        <div className="col-md-2">
                            <h2>Informace</h2>
                            <div className="heading-separator"></div>
                            <ul>
                                <li><a href="#">Cvičte s námi</a></li>
                                <li><a href="#">Tabata</a></li>
                                <li><a href="#">Jóga</a></li>
                            </ul>
                            <h2>Sponzoři</h2>
                            <div className="heading-separator"></div>
                        </div>
                        <div className="col-md-2">
                            <h2>Můj účet</h2>
                            <div className="heading-separator"></div>
                            <ul>
                                <li><a href="#">Moje adresy</a></li>
                                <li><a href="#">Moje objednávky</a></li>
                                <li><a href="#">Nákupní košík</a></li>
                            </ul>
                        </div>
                        <div className="col-md-3">
                            <h2>Sdílejte</h2>
                            <div className="heading-separator"></div>
                            <div className="social-sites">
                                <a href="#"><img src="/web/img/social-sites/fb.png" /></a>
                                <a href="#"><img src="/web/img/social-sites/pinterest.png" /></a>
                                <a href="#"><img src="/web/img/social-sites/instagram.png" /></a>
                                <a href="#"><img src="/web/img/social-sites/twitter.png" /></a>
                            </div>
                            <h2>Newsletter</h2>
                            <div className="heading-separator"></div>
                            <form action="" className="newsletter" method="post">
                                <div className="input-group">
                                    <input type="email" className="form-control" placeholder="E-mailová adresa" />
                        <span className="input-group-btn">
                            <button className="btn"><span className="fa fa-paper-plane"></span></button>
                        </span>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div className="bottom">
                    <div className="container">
                        <div className="col-md-6">
                            by: www.creativecompany.cz
                        </div>
                        <div className="col-md-6 text-right">
                            podporujeme: --1-- --2-- --3--
                        </div>
                    </div>
                </div>
            </footer>
        );
    }
};


export default Footer;
