const React = require('react');

// components
import Nav from './Nav.js';
import Footer from './Footer';

const MainComponent = (props) => {
    return (
        <div>
            <Nav />
            {props.children}
            <Footer  />
        </div>
    );
};


export default MainComponent;
