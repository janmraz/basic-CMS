const React = require('react');

class Header1 extends React.Component {
    render(){
        let items = this.props.stories ? this.props.stories.map((story,i) => {
            let cn = i === 0 ? "item active" : "item";
            return <div className={cn} key={i}>
                    <h3>{story.name}</h3>
                    <div className="heading-separator"></div>
                    <p>
                        {story.text}
                    </p>
            </div>;
        }) : <div> Loading...</div>;
        let links =  this.props.stories ? this.props.stories.map((story,i) => {
            return <li data-target="#first-carousel" key={i} data-slide-to={i} className={i === 0 ? "active" : ""}></li>;
        }) : <div> Loading...</div>;
        console.log('links',links);
        return (
            <section className="bg-white slides">
                <div className="container">
                    <div id="first-carousel" className="carousel slide" data-ride="carousel">
                        <div className="carousel-inner" role="listbox">
                            {items}
                        </div>
                        <ol className="carousel-indicators">
                            {links}
                        </ol>
                    </div>
                </div>
            </section>
        );
    }
};


export default Header1;
