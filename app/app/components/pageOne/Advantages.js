const React = require('react');

class Advantages extends React.Component {
    render(){
        return (
            <section className="bg-white">
                <div className="container steps">
                    <div className="col-md-4">
                        <div className="step">
                            <div className="heading">
                                <img src="web/img/hours-icon.png" />
                            </div>
                            <p className="subheader">
                                Šetří čas
                            </p>
                            <p>
                                {this.props.time}
                            </p>
                        </div>
                    </div>
                    <div className="col-md-4">
                        <div className="step">
                            <div className="heading">
                                <img src="web/img/cards-icon.png" />
                            </div>
                            <p className="subheader">
                                Šetří peníze
                            </p>
                            <p>
                                {this.props.money}
                            </p>
                        </div>
                    </div>
                    <div className="col-md-4">
                        <div className="step">
                            <div className="heading">
                                <img src="web/img/play-button-icon.png" />
                            </div>
                            <p className="subheader">
                                Lekce online
                            </p>
                            <p>
                                {this.props.lectures}
                            </p>
                        </div>
                    </div>
                </div>
            </section>
        );
    }
};


export default Advantages;