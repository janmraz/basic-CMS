const React = require('react');

class Header1 extends React.Component {
    render(){
        return (
            <header>
                <div className="container">
                    <h1>{this.props.header}</h1>
                    <h2>
                        {this.props.text}
                    </h2>
                    <a href="#" className="btn">Vyzkoušet</a>

                    <a href="#" className="page-scroll wow bounce animated" data-wow-delay="0s" data-wow-duration="1.25s" data-wow-iteration="infinite">
                        <img src="web/img/scroll-down.png" />
                    </a>
                </div>
            </header>
        );
    }
};


export default Header1;