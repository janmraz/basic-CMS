const React = require('react');

class Header1 extends React.Component {
    render(){
        let items = this.props.slides ? this.props.slides.map((slide,i) => {
            let cn = i === 0 ? "item active" : "item";
            return <div className={cn} key={i}>
                        <div className="container">
                            <div className="story">
                                <div className="row">
                                    <div className="col-md-3 profile">
                                    <img src={slide.image}/>
                                    <div className="name">{slide.name}</div>
                                    <div className="age">{slide.years}</div>
                                </div>
                                <div className="col-md-9">
                                    <h4>{slide.title}</h4>
                                    <div className="heading-separator"></div>
                                    <p>{slide.text}</p>
                                </div>
                            </div>
                            <a href="#" className="full-story-btn">Celý příběh</a>
                        </div>
                    </div>
                </div>
        }) : <div> Loading...</div>;
        return (
            <section className="bg-white stories">
                <div id="second-carousel" className="carousel slide" data-ride="carousel">
                    <div className="carousel-inner" role="listbox">
                        {items}
                    </div>

                    <a className="left carousel-control" href="#second-carousel" role="button" data-slide="prev">
                        <img src="web/img/carousel-left.png" />
                            <span className="sr-only">Previous</span>
                    </a>
                    <a className="right carousel-control" href="#second-carousel" role="button" data-slide="next">
                        <img src="web/img/carousel-right.png" />
                            <span className="sr-only">Next</span>
                    </a>
                </div>
            </section>
        );
    }
};


export default Header1;
