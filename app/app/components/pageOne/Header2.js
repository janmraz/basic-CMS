const React = require('react');

class Header2 extends React.Component {
    render(){
        return (
            <section className="bg-white">
                <div className="container">
                    <h3>{this.props.header}</h3>
                    <p>
                        {this.props.text}
                    </p>
                </div>
            </section>
        );
    }
};


export default Header2;