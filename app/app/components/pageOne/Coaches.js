const React = require('react');

class Coaches extends React.Component {
    render(){
        let coaches = this.props.coaches ? this.props.coaches.map((coach,i) => {
            if(i < 3){
            return <div className="col-md-4" key={i}>
                        <img src={coach.image} className="img-circle portrait" />
                        <div className="name">
                            {coach.name}
                        </div>
                        <div className="description">
                            {coach.text}
                        </div>
                        <a href="#" className="btn-action">eSchůzka s trenérem</a>
                    </div>
            }
        }) : <div>Loading...</div>;
        return (
            <section className="bg-white trainers">
                <div className="container info">
                    <small>Začněte vičit s naším on-line fitness <a href="#">Cvičzdomu</a></small>
                    <h3>{this.props.title}</h3>
                    <div className="heading-separator"></div>
                    <p>
                        {this.props.text}
                    </p>
                </div>
                <div className="people">
                    <div className="container">
                        <div className="row">
                            {coaches}
                        </div>
                    </div>
                </div>
            </section>
        );
    }
};


export default Coaches;
