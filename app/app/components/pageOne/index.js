const React = require('react');
import Advantages from './Advantages';
import Header1 from './Header1';
import Header2 from './Header2';
import Slider from './Slider';
import Stories from './Stories';
import Informations from './Informations';
import Coaches from './Coaches';

import { connect } from 'react-redux';
import { getConfig } from '../../actions/actions_config';


class PageOne extends React.Component {
    constructor(props){
        super(props);

    }
    componentDidMount(){
        this.props.dispatch(getConfig());
    }
    render(){
        return (
            <div>
                <div className="wrapper">
                    <Header1 header={this.props.config.main_header} text={this.props.config.main_header_text}/>
                    <Header2 header={this.props.config.second_header} text={this.props.config.second_header_text}/>
                    <section className="example-3">
                        <div className="container align-middle">
                            <div>
                                <a href="#" className="play-button"></a>
                            </div>
                        </div>
                    </section>
                    <Advantages time={this.props.config.save_time} money={this.props.config.save_money} lectures={this.props.config.lectures_online}/>
                    <section className="router">
                        <div className="container">
                            <div className="row">
                                <div className="col-md-4 col-xs-12">
                                    <a href="#" className="button"><span>registrace</span></a>
                                </div>

                                <div className="col-md-4 col-xs-12">
                                    <a href="#" className="button"><span>typy cvičení</span></a>
                                </div>

                                <div className="col-md-4 col-xs-12">
                                    <a href="#" className="button"><span>služby & doplňky</span></a>
                                </div>
                            </div>
                        </div>
                    </section>
                    <Slider slides={this.props.config.sliderInfo} />
                    <section className="bg-food">
                        <div className="container">
                            <a href="#" className="button white-bordered">Sestavit</a>
                        </div>
                    </section>
                    <Stories stories={this.props.config.stories}/>
                    <section className="bg-white videos">
                        <div className="claim">
                            <h3>Zaregistruj se a začni ihned cvičit</h3>
                        </div>
                        <div className="container">
                            <div className="row">
                                    <div className="col-md-6 col-md-offset-3 video">
                                        <img src="web/img/video.jpg" />
                                        <a href="#" className="play-button"></a>
                                    </div>
                            </div>
                            <div className="row">
                                <div className="col-md-6 video">
                                    <img src="web/img/video.jpg" />
                                    <a href="#" className="play-button"></a>
                                </div>
                                <div className="col-md-6 video">
                                    <img src="web/img/video.jpg" />
                                    <a href="#" className="play-button"></a>
                                </div>
                            </div>
                        </div>
                    </section>
                    <Informations text={this.props.config.information_text} title={this.props.config.information_header}/>
                    <Coaches coaches={this.props.config.coaches} text={this.props.config.coach_header} title={this.props.coach_subheader}/>
                </div>
            </div>
        );
    }
};


export default connect((state)=>{
    return {
        config: state.config,
    }
})(PageOne);
