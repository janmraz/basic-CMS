const React = require('react');

class Informations extends React.Component {
    render(){
        return (
            <section className="bg-purple">
                <div className="container">
                    <h3>{this.props.title}</h3>
                    <div className="heading-separator"></div>
                    <p>
                        {this.props.text}
                    </p>
                </div>
            </section>
        );
    }
};


export default Informations;
