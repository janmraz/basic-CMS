const React = require('react');
import { connect } from 'react-redux';
import { getConfig } from '../../actions/actions_config';


class PageOne extends React.Component {
    constructor(props){
        super(props);

    }
    componentDidMount(){
        this.props.dispatch(getConfig());
    }
    render(){
        return (
            <div className="wrapper">
                <header className="cviceni-s-detmi">
                    <div className="container">
                        <a href="#" className="play-button button-center"></a>
                    </div>
                </header>

                <section className="tabs">
                    <div className="row">
                        <div className="col-md-6 links">
                            <ul>
                                <li><a href="#" className="active" data-tab="tab_1">{this.props.config.titleOne}</a></li>
                                <li><a href="#" data-tab="tab_2">{this.props.config.titleTwo}</a></li>
                                <li><a href="#" data-tab="tab_3">{this.props.config.titleThree}</a></li>
                                <li><a href="#" data-tab="tab_4">{this.props.config.titleFour}</a></li>
                                <li><a href="#" data-tab="tab_5">{this.props.config.titleFive}</a></li>
                            </ul>

                        </div>
                        <div className="col-md-6 content">
                            <div className="tab active" id="tab_1">
                            </div>
                            <div className="tab" id="tab_2">

                            </div>
                            <div className="tab" id="tab_3">

                            </div>
                            <div className="tab" id="tab_4">

                            </div>
                            <div className="tab" id="tab_5">

                            </div>
                        </div>
                    </div>
                </section>

                <section className="info-photo">
                    <img src="/web/img/girl.png" className="photo bottom left" width="225" />
                        <div className="container">
                            <div className="row">
                                <div className="col-md-9 col-md-offset-3 text-center">
                                    <h3>{this.props.config.encourage_header}</h3>
                                    <p>{this.props.config.encourage_text}</p>
                                </div>
                            </div>
                        </div>
                </section>
            </div>
        );
    }
};


export default connect((state)=>{
    return {
        config: state.config,
    }
})(PageOne);
