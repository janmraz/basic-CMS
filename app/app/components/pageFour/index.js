const React = require('react');

import { connect } from 'react-redux';
import { getConfig } from '../../actions/actions_config';


class PageOne extends React.Component {
    constructor(props){
        super(props);

    }
    componentDidMount(){
        this.props.dispatch(getConfig());
    }
    render(){
        return (
            <div className="wrapper">

                <header className="nas-tym">
                    <div className="claims-bottom">
                        <div className="container">
                            <h1>{this.props.config.team_header}</h1>
                            <h2>
                                {this.props.config.team_text}
                            </h2>
                        </div>
                    </div>
                </header>

                <section className="bg-purple">
                    <div className="container">
                        <h3>{this.props.config.info_header}</h3>
                        <div className="heading-separator"></div>
                        <p>
                            {this.props.config.info_text}
                        </p>
                    </div>
                </section>

                <section className="bg-white">
                    <div className="container">
                        <h3>{this.props.config.uninfo_header}</h3>
                        <p>
                            {this.props.config.uninfo_text}
                        </p>
                    </div>
                </section>

                <section className="try-it">
                    <div className="container align-middle">
                        <div>
                            <a href="#" className="try-it-button">Vyzkoušet měsíc zdarma</a>
                        </div>
                    </div>
                </section>
            </div>
        );
    }
};


export default connect((state)=>{
    return {
        config: state.config,
    }
})(PageOne);
