const React = require('react');

import { connect } from 'react-redux';
import { getConfig } from '../../actions/actions_config';


class PageOne extends React.Component {
    constructor(props){
        super(props);

    }
    componentDidMount(){
        this.props.dispatch(getConfig());
    }
    render(){
        return (
            <div className="wrapper">
                Page five
            </div>
        );
    }
};


export default connect((state)=>{
    return {
        config: state.config,
    }
})(PageOne);
