import { combineReducers } from 'redux';

// reducers
import UserReducer from './reducer_user';
import AlertReducer from './reducer_alert';
import ConfigReducer from './reducer_config';

const rootReducer = combineReducers({
    user: UserReducer,
    alerts: AlertReducer,
    config: ConfigReducer
});

export default rootReducer;
