// Router set up
const React = require('react');
const {Route, Router, IndexRoute, browserHistory} = require('react-router');
let ls = require('local-storage');

// Components
import Main from '../components/App/Main';
import PageOne from '../components/pageOne';
import PageTwo from '../components/pageTwo';
import PageThree from '../components/pageThree';
import PageFour from '../components/pageFour';
import PageFive from '../components/pageFive';


// Routes
const routes = (
    <Router history={browserHistory}>
        <Route path='/' component={Main}>
            <IndexRoute component={PageOne} />
            <Route path='typy-cviceni' component={PageTwo} />
            <Route path='cviceni-s-detmi' component={PageThree} />
            <Route path='nas-tym' component={PageFour} />
            <Route path='kontakty' component={PageFive} />
        </Route>
    </Router>
);


export default routes ;
