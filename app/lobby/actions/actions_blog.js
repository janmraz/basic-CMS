import axios from 'axios';

import { GET_ARTICLES,GET_ARTICLE } from './types';

export function getArticles() {
    return function(dispatch) {
        return axios.get('/api/user/articles')
            .then((response) => {
                console.log(response.data);
                dispatch({
                    type: GET_ARTICLES,
                    payload: response.data
                });
            })
            .catch((err) => {
                console.error("ERR",err);
            });
    };
}
export function getArticle(id) {
    return function(dispatch) {
        return axios.get('/api/user/article/'+id)
            .then((response) => {
                console.log(response.data);
                dispatch({
                    type: GET_ARTICLE,
                    payload: response.data
                });
            })
            .catch((err) => {
                console.error("ERR",err);
            });
    };
}