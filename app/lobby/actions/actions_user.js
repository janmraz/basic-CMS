import axios from 'axios';

import { USER_LOGIN, GET_INFO } from './types';

export function logInUser(data) {
    return function(dispatch) {
        return axios.post('/api/user/signin', data)
            .then((response) => {
                console.log('response from login',response);
                dispatch({
                    type: USER_LOGIN,
                    payload: response.data
                });
            })
            .catch((err) => {
                console.error("ERR",err);
            });
    };
}
export function getInfo(id) {
    return function(dispatch) {
        return axios.get('/api/user/'+id)
            .then((response) => {
                console.log('response from get info',response.data);
                dispatch({
                    type: GET_INFO,
                    payload: response.data
                });
            })
            .catch((err) => {
                console.error("ERR",err);
            });
    };
}