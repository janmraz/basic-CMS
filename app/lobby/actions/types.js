// USER
export const USER_LOGIN = 'USER_LOGIN';
export const GET_INFO = 'GET_INFO';

// ALERT
export const NEW_ALERT = 'NEW_ALERT';
export const REMOVE_ALERT = 'REMOVE_ALERT';

//BLOG
export const GET_ARTICLES = 'GET_ARTICLES';
export const GET_ARTICLE = 'GET_ARTICLE';
