import { combineReducers } from 'redux';

// reducers
import UserReducer from './reducer_user';
import AlertReducer from './reducer_alert';
import ConfigReducer from './reducer_config';
import BlogReducer from './reducer_blog';
import ArticleReducer from './reducer_article';

const rootReducer = combineReducers({
    user: UserReducer,
    alerts: AlertReducer,
    config: ConfigReducer,
    blog: BlogReducer,
    article: ArticleReducer
});

export default rootReducer;
