import { USER_LOGIN, GET_INFO } from '../actions/types.js';
let ls = require('local-storage');


export default function(state = {}, { type, payload }) {
    if (type === USER_LOGIN) {
        ls('token',payload.token);
        console.log('userid',payload.data._id);
        ls('user.id',payload.data._id);
        location.reload();
        return payload.data
    }
    if(type === GET_INFO){
        return payload.data
    }
    return state;
}

