import { GET_ARTICLE } from '../actions/types.js';


export default function(state = {}, { type, payload }) {
    if (type === GET_ARTICLE) {
        return payload
    }
    return state;
}

