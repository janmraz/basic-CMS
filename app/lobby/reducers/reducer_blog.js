import { GET_ARTICLES } from '../actions/types.js';


export default function(state = [], { type, payload }) {
    if (type === GET_ARTICLES) {
        return payload
    }
    return state;
}

