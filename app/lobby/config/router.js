// Router set up
const React = require('react');
const {Route, Router, IndexRoute, browserHistory} = require('react-router');
let ls = require('local-storage');

// Components
import Auth from '../components/Auth/Auth';
import Home from '../components/Admin/Home';

import Index from '../components/Index';
import Categories from '../components/Categories';
import Blog from '../components/Blog';
import Article from '../components/Blog/Article';
import Calendar from '../components/Calendar';
import Messages from '../components/Messages';

// Routes
const routes = (
    <Router history={browserHistory}>
        <Route path='*' component={Auth} />
    </Router>
);

const authorizedRoutes = (
    <Router history={browserHistory}>
        <Route path='lobby' component={Home} >
            <IndexRoute component={Index}/>
            <Route path='categories' component={Categories}/>
            <Route path='blog' component={Blog}/>
            <Route path='/lobby/blog/:id' component={Article} />
            <Route path='calendar' component={Calendar}/>
            <Route path='messages' component={Messages}/>
        </Route>
    </Router>
);


export default ls('token') === null ? routes : authorizedRoutes;
