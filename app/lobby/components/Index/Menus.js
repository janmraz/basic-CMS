const React = require('react');
const {Link, IndexLink} = require('react-router');

class Menus extends React.Component {

    render() {
        return (
            <aside className="food-plan">
                <div className="content__header content__header--center">
                    <div className="content__header__heading">Jídelníčky</div>
                </div>

                <div className="food-menu">
                    <a href="#" className="food-menu__item">
                        <div className="menu">
                            <div className="menu__overlay">
                                <span className="menu__name">Snídaně</span>
                                <p className="menu__description">
                                    Naučte se jíst zdravě od pondělí do neděle.
                                </p>
                                <div className="menu__author">
                                    <img src="/assets/build/img/menu-author.jpg" alt="" title=""/> <span>Přidáno: Karel B.</span>
                                </div>
                            </div>
                        </div>
                    </a>

                    <a href="#" className="food-menu__item">
                        <div className="menu">
                            <div className="menu__overlay">
                                <span className="menu__name">Snídaně</span>
                                <p className="menu__description">
                                    Naučte se jíst zdravě od pondělí do neděle.
                                </p>
                                <div className="menu__author">
                                    <img src="/assets/build/img/menu-author.jpg" alt="" title=""/> <span>Přidáno: Karel B.</span>
                                </div>
                            </div>
                        </div>
                    </a>

                    <a href="#" className="food-menu__item">
                        <div className="menu">
                            <div className="menu__overlay">
                                <span className="menu__name">Snídaně</span>
                                <p className="menu__description">
                                    Naučte se jíst zdravě od pondělí do neděle.
                                </p>
                                <div className="menu__author">
                                    <img src="/assets/build/img/menu-author.jpg" alt="" title=""/> <span>Přidáno: Karel B.</span>
                                </div>
                            </div>
                        </div>
                    </a>

                    <a href="#" className="food-menu__item">
                        <div className="menu">
                            <div className="menu__overlay">
                                <span className="menu__name">Snídaně</span>
                                <p className="menu__description">
                                    Naučte se jíst zdravě od pondělí do neděle.
                                </p>
                                <div className="menu__author">
                                    <img src="/assets/build/img/menu-author.jpg" alt="" title="" /> <span>Přidáno: Karel B.</span>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>

                <div className="content__header content__header--center">
                    <div className="content__header__heading">Aktivní prvky</div>
                </div>

                <div className="active-links">
                    <Link to="/lobby/calendar" className="active-links__item">
                        <div className="bg"><img src="/assets/build/img/calendar-interface-symbol-tool.svg" alt="" title="" /></div>
                    </Link>

                    <Link to="/lobby/messages" className="active-links__item">
                        <div className="bg"><img src="/assets/build/img/speech-bubble-black.svg" alt="" title="" /></div>
                    </Link>
                </div>
            </aside>
        );
    }
}

export default Menus;
