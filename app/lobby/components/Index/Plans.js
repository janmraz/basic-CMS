const React = require('react');
const {Link, IndexLink} = require('react-router');

class Plans extends React.Component {

    render() {
        return (
            <main className="daily-plan">
                <div className="content__header">
                    <div className="content__header__toolbox">
                        <Link to="/lobby/categories" className="button button--primary">Kategorie</Link>
                    </div>
                    <div className="content__header__heading">Dnešní plány</div>
                </div>

                <div className="daily-plans">
                    <div className="daily-plans__item">
                        <div className="daily-plans__item__info">
                            <div className="daily-plans__item__info__lector">
                                <img src="/assets/build/img/lector-photo.jpg" alt="" title=""/>

                                <span className="lector__name">S Monikou Krajčovou</span>
                                <p className="lector__description">specialistka se zaměřením na problemové partie</p>
                            </div>
                            <div className="daily-plans__item__info__lection">
                                <span className="lection__name">Cvičení nohou</span>

                                <span className="lection__motto">Lorem ipsum dolor sit amet, consect.</span>

                                <p className="lection__description">Fusce consectetuer risus a nunc. Duis condimentum augue id sit amet, tempor quis. Integer malesuada.</p>
                            </div>
                        </div>
                        <a href="#" className="daily-plans__item__button">Přidat&nbsp;se do&nbsp;lekce</a>
                    </div>

                    <div className="daily-plans__item">
                        <div className="daily-plans__item__info">
                            <div className="daily-plans__item__info__lector">
                                <img src="/assets/build/img/lector-photo.jpg" alt="" title=""/>

                                <span className="lector__name">S Monikou Krajčovou</span>
                                <p className="lector__description">specialistka se zaměřením na problemové partie</p>
                            </div>
                            <div className="daily-plans__item__info__lection">
                                <span className="lection__name">Cvičení nohou</span>

                                <span className="lection__motto">Lorem ipsum dolor sit amet, consect.</span>

                                <p className="lection__description">Fusce consectetuer risus a nunc. Duis condimentum augue id sit amet, tempor quis. Integer malesuada.</p>
                            </div>
                        </div>
                        <a href="#" className="daily-plans__item__button">Přidat&nbsp;se do&nbsp;lekce</a>
                    </div>

                    <div className="daily-plans__item">
                        <div className="daily-plans__item__info">
                            <div className="daily-plans__item__info__lector">
                                <img src="/assets/build/img/lector-photo.jpg" alt="" title=""/>

                                <span className="lector__name">S Monikou Krajčovou</span>
                                <p className="lector__description">specialistka se zaměřením na problemové partie</p>
                            </div>
                            <div className="daily-plans__item__info__lection">
                                <span className="lection__name">Cvičení nohou</span>

                                <span className="lection__motto">Lorem ipsum dolor sit amet, consect.</span>

                                <p className="lection__description">Fusce consectetuer risus a nunc. Duis condimentum augue id sit amet, tempor quis. Integer malesuada.</p>
                            </div>
                        </div>
                        <a href="#" className="daily-plans__item__button">Přidat&nbsp;se do&nbsp;lekce</a>
                    </div>
                </div>
            </main>
        );
    }
}

export default Plans;
