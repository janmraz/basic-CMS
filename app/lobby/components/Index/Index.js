const React = require('react');
const {Link, IndexLink} = require('react-router');
import { connect } from 'react-redux';
import Menus from './Menus';
import Plans from './Plans';

class Index extends React.Component {
    render() {
        return (
            <div className="content">
                <Plans />
                <Menus />
            </div>
        );
    }
}
export default Index;
