const React = require('react');
const {Link, IndexLink} = require('react-router');
import { connect } from 'react-redux';

import { getArticles } from '../../actions/actions_blog';


class Blog extends React.Component {
    componentDidMount(){
        this.props.dispatch(getArticles());
    }
    render() {
        let articles = this.props.articles.map((a,i)=>{
            return <article className="article" key={i}>
                <div className="article__author">
                    <div className="article__author__photo">
                        <img src={a.user_photo} alt="" title="" />
                    </div>

                    <div className="article__author__info">
                        <span className="article__author__name">{a.user_name}</span>
                        <span className="article__date">{a.created}</span>
                    </div>
                </div>
                <div className="article__content">
                    <span className="article__name">{a.header}</span>

                    <div className="article__photo">
                        <img src={a.photo} alt="" title="" />
                    </div>

                    <p className="article__excerpt">
                        {a.text}
                    </p>

                    <Link to={"/lobby/blog/"+ a._id} className="article__read-more">číst více</Link>
                </div>
            </article>
        });
        return (
            <div className="content">
                <main className="content__blog">
                    <div className="content__header">
                        <div className="content__header__toolbox">
                            <Link to="/lobby" className="button button--back">Zpět</Link>
                        </div>
                        <div className="content__header__heading">Blog</div>
                    </div>

                    <div className="blog">
                        {articles}
                    </div>
                </main>
                </div>
        );
    }
}
export default connect((state)=>{
    return {
        articles: state.blog,
    }
})(Blog);
