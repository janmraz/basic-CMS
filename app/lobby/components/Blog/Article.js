const React = require('react');
const {Link, IndexLink} = require('react-router');
import { connect } from 'react-redux';

import { getArticle } from '../../actions/actions_blog';


class Blog extends React.Component {
    componentDidMount(){
        let id = this.props.params.id;
        console.log('id',id);
        this.props.dispatch(getArticle(id));
    }
    render() {
        let { header, photo, text, user_name, user_photo, created,} = this.props.article;
    return (
        <div className="content">
            <main className="content__blog">
                <div className="content__header">
                    <div className="content__header__toolbox">
                        <Link to="/lobby/blog" className="button button--back">Zpět</Link>
                    </div>
                    <div className="content__header__heading">Blog</div>
                </div>

                <div className="article-detail">
                    <article className="article">
                        <div className="article__author">
                            <div className="article__author__photo">
                                <img src={user_photo} alt="" title="" />
                            </div>

                            <div className="article__author__info">
                                <span className="article__author__name">{user_name}</span>
                                <span className="article__author__motto">{created}</span>
                            </div>
                        </div>
                        <div className="article__content">
                            <span className="article__name">{header}</span>

                            <div className="article__photo">
                                <img src={photo} alt="" title="" />
                            </div>

                            <p>
                                {text}
                            </p>

                        </div>
                    </article>

                </div>
            </main>
        </div>
    );
}
}
export default connect((state)=>{
    return {
    article: state.article,
}
})(Blog);
