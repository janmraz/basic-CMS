const React = require('react');
const {Link, IndexLink} = require('react-router');


class Categories extends React.Component {
    render() {
        return (
            <div className="content">
                    <main className="content__categories">
                        <div className="content__header">
                            <div className="content__header__toolbox">
                                <Link to="lobby" className="button button--back">Zpět</Link>
                            </div>
                        </div>

                        <div className="categories">
                            <div className="category">
                                <div className="category__bg">
                                    <a className="category__caption">
                                        Jóga
                                    </a>
                                </div>
                            </div>
                            <div className="category">
                                <div className="category__bg">
                                    <a className="category__caption">
                                        Jóga
                                    </a>
                                </div>
                            </div>
                            <div className="category">
                                <div className="category__bg">
                                    <a className="category__caption">
                                        Jóga
                                    </a>
                                </div>
                            </div>

                            <div className="category">
                                <div className="category__bg">
                                    <a className="category__caption">
                                        Jóga
                                    </a>
                                </div>
                            </div>
                            <div className="category">
                                <div className="category__bg">
                                    <a className="category__caption">
                                        Jóga
                                    </a>
                                </div>
                            </div>
                            <div className="category">
                                <div className="category__bg">
                                    <a className="category__caption">
                                        Jóga
                                    </a>
                                </div>
                            </div>

                            <div className="category">
                                <div className="category__bg">
                                    <a className="category__caption">
                                        Jóga
                                    </a>
                                </div>
                            </div>
                            <div className="category">
                                <div className="category__bg">
                                    <a className="category__caption">
                                        Jóga
                                    </a>
                                </div>
                            </div>
                            <div className="category">
                                <div className="category__bg">
                                    <a className="category__caption">
                                        Jóga
                                    </a>
                                </div>
                            </div>
                        </div>
                    </main>
                </div>
        );
    }
}
export default Categories;
