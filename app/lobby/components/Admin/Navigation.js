const React = require('react');
const {Link, IndexLink} = require('react-router');
const ls = require('local-storage');

class Navigation extends React.Component {

    render() {
        return (
            <div className="user-menu">
                <ul className="user-menu__links">
                    <li onClick={()=>{document.getElementById("toggler").click()}}>
                        <Link to="/lobby">Stav účtu</Link>
                    </li>
                    <li onClick={()=>{document.getElementById("toggler").click()}}>
                        <Link to="/lobby">Zakoupit členství / kredity</Link>
                    </li>
                    <li onClick={()=>{document.getElementById("toggler").click()}}>
                        <Link to="/lobby">Rozvrh lekcí / Moje lekce</Link>
                    </li>
                    <li onClick={()=>{document.getElementById("toggler").click()}}>
                        <Link to="/lobby">Jídelníček ma míru</Link>
                    </li>
                    <li onClick={()=>{document.getElementById("toggler").click()}}>
                        <Link to="/lobby/messages">Zprávy</Link>
                    </li>
                    <li onClick={()=>{document.getElementById("toggler").click()}}>
                        <Link to="/lobby">Upravit profil</Link>
                    </li>
                    <li onClick={()=>{document.getElementById("toggler").click()}}>
                        <Link to="/lobby">Historie transakcí</Link>
                    </li>
                    <li onClick={()=>{document.getElementById("toggler").click()}}>
                        <Link to="/lobby">Upravit profil</Link>
                    </li>
                    <li onClick={()=>{document.getElementById("toggler").click()}}>
                        <Link to="/lobby">Technická podpora</Link>
                    </li>
                    <li onClick={()=>{document.getElementById("toggler").click()}}>
                        <Link to="/lobby/blog">Blog</Link>
                    </li>
                    <li className="user-menu__links__link--bold"><a onClick={() => {ls('token',null);ls('isAdmin',null);window.location = '/';}}>Odhlásit</a></li>
                </ul>
            </div>
        );
    }
}

export default Navigation;
