const React = require('react');
const {Link, IndexLink} = require('react-router');
import { connect } from 'react-redux';
import Navigation from './Navigation';
import Header from '../Admin/Header';

class Home extends React.Component {
    render() {
        return (
            <div className="hold-transition skin-red sidebar-mini">
            <div className="admin-wrapper">
                <div className="wrapper">
                    <Header />
                    <div className="wrapper__main">
                        {this.props.children}
                    </div>
                </div>
                <Navigation/>

            </div>
            </div>
        );
    }
}
export default Home;
