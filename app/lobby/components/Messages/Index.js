const React = require('react');
const {Link, IndexLink} = require('react-router');


class Messages extends React.Component {
    render() {
        return (
            <div className="content">
                <main className="content__messages">
                    <div className="content__header">
                        <div className="content__header__toolbox">
                            <Link to="lobby" className="button button--back">Zpět</Link>
                        </div>
                    </div>

                    <div className="messages">
                        <div className="messages__sidebar">
                            <a href="#" className="button button--new-message">+ Nová zpráva</a>
                        </div>
                        <div className="messages__list">
                            <div className="messages__form">
                                <form action="" method="post">
                                    <div className="messages__form__group">
                                        <input title="Vaše zpráva..." type="text" name="message"
                                               className="messages__form__text"/>
                                        <button className="messages__form__send" name="send">
                                            <svg xmlns="http://www.w3.org/2000/svg" version="1.1" id="Capa_1" x="0px"
                                                 y="0px" viewBox="0 0 486.736 486.736"
                                                 style={{"enableBackground": "new 0 0 486.736 486.736;"}}
                                                 xmlSpace="preserve" width="512px" height="512px">
                                                <g>
                                                    <path
                                                        d="M481.883,61.238l-474.3,171.4c-8.8,3.2-10.3,15-2.6,20.2l70.9,48.4l321.8-169.7l-272.4,203.4v82.4c0,5.6,6.3,9,11,5.9   l60-39.8l59.1,40.3c5.4,3.7,12.8,2.1,16.3-3.5l214.5-353.7C487.983,63.638,485.083,60.038,481.883,61.238z"
                                                        fill="#FFFFFF"/>
                                                </g>
                                            </svg>
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </main>
            </div>
        );
    }
}
export default Messages;
