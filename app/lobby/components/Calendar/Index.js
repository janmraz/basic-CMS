const React = require('react');
const {Link, IndexLink} = require('react-router');


class Calendar extends React.Component {
    render() {
        return (
            <div className="content">
                <main className="content__calendar">
                    <div className="content__header">
                        <div className="content__header__toolbox">
                            <Link to="lobby" className="button button--back">Zpět</Link>
                        </div>
                        <div className="content__header__heading">Kalendář</div>
                    </div>

                    <div className="calendar">
                        <div className="calendar__month">
                            <div className="calendar__week">
                                <div className="calendar__day">
                                    <a href="#">1</a>
                                </div>
                                <div className="calendar__day">
                                    <a href="#">2</a>

                                </div>
                                <div className="calendar__day">
                                    <a href="#">3</a>

                                </div>
                                <div className="calendar__day">
                                    <a href="#">4</a>

                                </div>
                                <div className="calendar__day">
                                    <a href="#">5</a>

                                </div>
                                <div className="calendar__day">
                                    <a href="#">6</a>

                                </div>
                                <div className="calendar__day">
                                    <a href="#">7</a>
                                </div>
                            </div>

                            <div className="calendar__week">
                                <div className="calendar__day">
                                    <a href="#">8</a>
                                </div>
                                <div className="calendar__day">
                                    <a href="#">9</a>

                                </div>
                                <div className="calendar__day">
                                    <a href="#">10</a>

                                </div>
                                <div className="calendar__day">
                                    <a href="#">11</a>

                                </div>
                                <div className="calendar__day">
                                    <a href="#">12</a>

                                </div>
                                <div className="calendar__day">
                                    <a href="#">13</a>

                                </div>
                                <div className="calendar__day">
                                    <a href="#">14</a>
                                </div>
                            </div>

                            <div className="calendar__week">
                                <div className="calendar__day">
                                    <a href="#">15</a>
                                </div>
                                <div className="calendar__day">
                                    <a href="#">16</a>

                                </div>
                                <div className="calendar__day">
                                    <a href="#">17</a>

                                </div>
                                <div className="calendar__day">
                                    <a href="#">18</a>

                                </div>
                                <div className="calendar__day">
                                    <a href="#">19</a>

                                </div>
                                <div className="calendar__day">
                                    <a href="#">20</a>

                                </div>
                                <div className="calendar__day">
                                    <a href="#">21</a>
                                </div>
                            </div>
                            <div className="calendar__week">
                                <div className="calendar__day">
                                    <a href="#">22</a>
                                </div>
                                <div className="calendar__day">
                                    <a href="#">22</a>

                                </div>
                                <div className="calendar__day">
                                    <a href="#">23</a>

                                </div>
                                <div className="calendar__day">
                                    <a href="#">24</a>

                                </div>
                                <div className="calendar__day">
                                    <a href="#">25</a>

                                </div>
                                <div className="calendar__day">
                                    <a href="#">26</a>

                                </div>
                                <div className="calendar__day">
                                    <a href="#">27</a>
                                </div>
                            </div>

                            <div className="calendar__week">
                                <div className="calendar__day">
                                    <a href="#">28</a>
                                </div>
                                <div className="calendar__day">
                                    <a href="#">29</a>

                                </div>
                                <div className="calendar__day">
                                    <a href="#">30</a>

                                </div>
                                <div className="calendar__day">
                                    <a href="#">31</a>

                                </div>
                                <div className="calendar__day">
                                    &nbsp;
                                </div>
                                <div className="calendar__day">
                                    &nbsp;
                                </div>
                                <div className="calendar__day">
                                    &nbsp;
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="content__header content__header--center content__header--padded">
                        <div className="content__header__heading">Detail lekce</div>
                    </div>
                    <div className="calendar">
                        <div className="calendar__event">
                            <div className="calendar__event__item__info">
                                <div className="calendar__event__item__info__lector">
                                    <img src="/assets/build/img/lector-photo.jpg" alt="" title="" />

                                        <span className="lector__name">S Monikou Krajčovou</span>
                                        <p className="lector__description">specialistka se zaměřením na problemové partie</p>
                                </div>
                                <div className="calendar__event__item__info__lection">
                                    <span className="lection__name">Cvičení nohou</span>

                                    <span className="lection__motto">Lorem ipsum dolor sit amet, consect.</span>

                                    <p className="lection__description">Fusce consectetuer risus a nunc. Duis condimentum augue id sit amet, tempor quis. Integer malesuada.</p>
                                </div>
                            </div>
                            <a href="#" className="calendar__event__item__button">Přidat&nbsp;se do&nbsp;lekce</a>
                        </div>
                    </div>
                </main>
            </div>
        );
    }
}
export default Calendar;
